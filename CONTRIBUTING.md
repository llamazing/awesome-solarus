# Contributing to this list

This 'awesome' list is inspired by the famous [awesome lists](https://github.com/sindresorhus/awesome), which are curated list about one topic. It means that we (repositories maintainers) examine contributions and accept only the _relevant_ ones. _Relevant_ means that the project is well enough advanced and is ready to use by another user (for example, a Solarus Lua script that doesn't work or that doesn't follow good development practices won't be added here.)

## Steps

1. Fork the project.
2. Clone your version and create a branch, where you add your edit(s).
3. Push your branch.
4. Create a Merge Request into master of our repository.
5. We'll review your MR and ask for changes, and accept it (or not).

## Remarks

- This list is not versioned.
- This is not a place to advertize your project. Please be neutral in the description of your project. Add your game only if it's complete or almost complete.
- Don't create another file: write directly in the `README.md` file.
- Follow [standard Markdown writing convention](https://github.com/DavidAnson/markdownlint/blob/master/doc/Rules.md). You can install a linter to format the file automatically.
- Add your scripts/sprites/tilesets/... to the `Quest Development Resources` part. Create sub-titles if necessary.
- We prefer free and open-source projects.
