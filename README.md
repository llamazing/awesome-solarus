# Awesome Solarus [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

[<img src="images/solarus_logo.svg" align="right" width="200">](https://www.qt.io)

> A curated list of awesome tools, libraries, and resources for the Solarus game engine.

[Solarus](https://www.solarus-games.org) is a free and open-source 2D game engine made in C++. It is multiplatform and has a Lua API, which you make your games with.

This list is inspired by all the [awesome lists](https://github.com/sindresorhus/awesome). Contributions are welcome!

## Contents

- [Official Resources](#official-resources)
- [Other Solarus Resources](#other-solarus-resources)
- [Community](#community)
- [Quests](#quests)
- [Quest Development Resources](#quest-development-resources)
- [Other Game Development Resources](#other-game-development-resources)
- [Other Relevant Awesome Lists](#other-relevant-awesome-lists)
- [License](#license)

## Official Resources

- [Official Website](https://www.solarus-games.org) - The official website for the Solarus engine, with downloads, news, games and more.
- [Source Code](https://gitlab.com/solarus-games) - Browse the various repositories that comprise the Solarus project.
- [Documentation](https://www.solarus-games.org/doc/latest/) - Official Solarus Quest Maker documentation for those who want to make games with Solarus.
- [Tutorial](https://www.solarus-games.org/en/development/tutorials/solarus-official-guide) - Official tutorial, work in progress.
- [Video Tutorial](https://www.youtube.com/watch?v=8StwujI-Hbg&list=PLzJ4jb-Y0ufxwkj7IlfURcvCxaDCSecJY) - Solarus creator's video tutorial. NB: some made with an old version of Solarus, but still relevant.

## Other Solarus Resources

- [Book](https://zelphor-solarus-book.000webhostapp.com/Solarus-ARPG-Game-Development-Book_2.html) - Outdated because it covers Solarus 1.5 but still a very useful step-by-step detailed tutorial on how to use Solarus

## Community

- [Discord](https://discord.gg/yYHjJHt) - Official Discord server for asking for help or contributing to the engine development.
- [Forum](http://forum.solarus-games.org/) - Official forum, to ask for help, present your project and talk with the community.

## Quests

The official website lists [all the Solarus quests](https://www.solarus-games.org/en/games) that are published or whose development is far advanced.

### Released

- [The Legend of Zelda: Mystery of Solarus DX](https://gitlab.com/solarus-games/zsdx) - The sequel to *A Link to the Past* (1992, Super Nintendo), and the first game made with Solarus.
- [The Legend of Zelda: Mystery of Solarus XD](https://gitlab.com/solarus-games/zsxd) - A short parodic game that makes fun of *Zelda* tropes, initially released as an April's fools.
- [Tunics!](https://github.com/Legofarmen/tunics) - A randomly generated Rogue-Like in the *Zelda* universe.
- [The Legend of Zelda: Book of Mudora](https://github.com/wrightmat/zbom) - Stop the Dark Tribe from destroying Hyrule by obtaining the power of the Book of Mudora.
- [The Legend of Zelda: Return of the Hylian SE](https://gitlab.com/solarus-games/zelda-roth-se) - A Solarus-made remake of Vincent Jouillat's first game, and first episode of a trilogy.
- [Le Défi de Zeldo - Ch. 1 : La Revanche du Bingo](https://github.com/ZeldoRetro/defi_zeldo_chap_1) - A character named Zeldo challenges Link in a dungeon he created.
- [Le Défi de Zeldo - Ch. 2 : La Tour des Souvenirs](https://github.com/ZeldoRetro/defi_zeldo_chap_2) - Sequel of Ch. 1, Link must climb up to the top of the Memories Tower to fight Zeldo again.
- [Yarntown](https://github.com/MaxMraz/yarntown) - Bloodborne meets 2D Zelda: cursed but cute.

### In development

Please check licensing before using any of these projects scripts/assets in your projects.

- [Children of Solarus](https://gitlab.com/solarus-games/children-of-solarus) - A 100% free and open-source remake of *Mystery of Solarus DX*.
- [Ocean's Heart](https://github.com/MaxMraz/oceans-heart) - Navigate a world brimming with lawless pirates, dangerous monsters, and ancient magic.
- [Return to Soleil](https://gitlab.com/diegopau/return-to-soleil) - A *Soleil* fangame (*Crusader of Centy* in the US), released on 1994 for the Sega MegaDrive.
- [The Legend of Zelda: A Link to the Dream](https://gitlab.com/zeldaforce/zelda-alttd) - Remake of the GameBoy cult classic *The Legend of Zelda: Link's Awakening*.
- [The Legend of Zelda: Mercuris Chest](https://gitlab.com/solarus-games/zelda-mercuris-chest) - Solarus Team ambitious project: a huge open-world kingdom that would showcase all Solarus capabilities.
- [The Legend of Zelda: Onilink Begins SE](https://gitlab.com/solarus-games/zelda-olb-se) - The remake of Vincent Jouillat's second game. Richer, bigger and harder than the first one.
- [Vegan on a Desert Island](https://gitlab.com/voadi/voadi) - Cheeky adventure game about activism and animal liberation.

## Quest Development Resources

Solarus resources (scripts, tilesets, sprites, sounds, etc.).

### Scripts

- [Visual Novel System](https://gitlab.com/ShargonPendragon/visual-novel-system) - Adds visual novel and dialog box customization options to Solarus.
- [Pathfinder](https://gitlab.com/llamazing/pathfinder) - Your hero will follow the most optimal path between the current position and a selected destination.

### Resource packs

- [The Legend of Zelda: A Link to the Past](https://www.solarus-games.org/en/development/resource-packs/the-legend-of-zelda-a-link-to-the-past) - All you need to recreate a game in *ALTTP* style. License: Proprietary (Fair use) for Nintendo rips, GPL v3 for the rest.
- [Solarus Free Resource Pack](https://www.solarus-games.org/en/development/resource-packs/free-resource-pack) - Contains only GPL and CC-BY-SA-licensed elements, scripts and resources in various styles, and my be used to bootstrap your first game.
- [Solarus MIT Starter Quest](https://gitlab.com/Splyth/solarus-mit-starter-quest/-/tree/master/data) - Contains only MIT-licensed scripts and Public Domain art assets, which means no GPL constraints. Useful for closed-source quests.

## Other Game Development Resources

More general tools and resources.

### Sprites & Tilesets

- [The Spriters Resource](https://www.spriters-resource.com/) - Huge database of sprites from games (retro and modern) from all kind of consoles and systems. NB: license if proprietary.
- [OpenGameArt](https://opengameart.org/) - Many Open Source resources here, both tilesets and sprites, with license filter. Contains 32x32 and 16x16 tilesets (more suited to Solarus).
- [Hero Generator](http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/) - This generates spritesheets for your hero. Very customizable.

### Game design

- [VGMaps - The VideoGame Atlas](https://vgmaps.com/) - Here you will find maps (pngs of full maps of each area) for many retrogames. This could be helpful to look at famous ARPGs of the 8-bit or 16-bit era and analyze their map design.

## Other Relevant Awesome Lists

- [Awesome Lua](https://github.com/LewisJEllis/awesome-lua) - You can use any Lua library to expand Solarus' own Lua API.
- [Awesome C++](https://github.com/fffaraz/awesome-cpp) - If you want to contribute to the engine development, this might interest you.
- [Awesome Qt](https://github.com/JesseTG/awesome-qt) - The Solarus Quest Editor and Solarus Launcher apps are made with the Qt Framework.
- [Awesome Game Development](https://github.com/ellisonleao/magictools) - Lots of tools to help you developing your Solarus quest.

## License

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0)

This list is in public domain.
